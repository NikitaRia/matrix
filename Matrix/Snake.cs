﻿namespace matrix;

public class Snake
{
    
    public void Snakes(int [,] array)
    {

        int limitRight = array.GetLength(1);
        int zeroIncrement = 0;
        int indicesDown = array.GetLength(1) - 1;
        int fromDown = 1;
        int limitDown = array.GetLength(0);
        int rowLeft = array.GetLength(0) - 1;
        int fromLeft = array.GetLength(1) - 2;
        int fromUp = array.GetLength(0) - 2;

        int quantityIndex = array.GetLength(0) * array.GetLength(1);
        int count = 0;

        Console.ForegroundColor = ConsoleColor.Magenta;
        Console.WriteLine();
        Console.WriteLine("Snake made of numbers\n");
        Console.ForegroundColor= ConsoleColor.Blue;

        while (count < quantityIndex)
        {
            for /*1 right*/ (int i = zeroIncrement; i < limitRight; i++)
            {
                count++;
                Console.Write($"{array[zeroIncrement, i]}  ");
            }
            limitRight--;

            for /*2 down*/ (int i = fromDown; i < limitDown; i++)
            {
                count++;
                Console.Write($"{array[i, indicesDown]}  ");
            }
            fromDown++;
            limitDown--;
            indicesDown--;

            for /*3 left*/ (int i = fromLeft; i >= zeroIncrement; i--)
            {
                count++;
                Console.Write($"{array[rowLeft, i]}  ");
            }
            rowLeft--;
            fromLeft--;

            for /*4 up*/ (int i = fromUp; i > zeroIncrement; i--)
            {
                count++;
                Console.Write($"{array[i, zeroIncrement]}  ");
            }
            fromUp--;
            zeroIncrement++;
        }

        Console.WriteLine("\n");
    }
}
