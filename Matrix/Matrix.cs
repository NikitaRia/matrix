﻿namespace matrix;

public class Matrix
{
    public void InputValidation()
    {
        Console.ForegroundColor = ConsoleColor.Yellow;
        Console.WriteLine("Enter size array..\n");
        Console.Write("Enter first value\t");

        string data = Console.ReadLine();
        int sizeDiagonal;

        if (int.TryParse(data, out sizeDiagonal))
        {
            if (sizeDiagonal <= 0 )
            {
                Console.WriteLine("\nSize cannot be <= 0");
                Console.ReadKey();
            }
        }
        else
        {
            Console.WriteLine("\nError: you can only enter numbers");
            Console.ReadKey();
        }

        Console.Write("\nEnter second value\t");
        string data_2 = Console.ReadLine();
        Console.WriteLine("\n");
        int sizeHorizontal;

        if (int.TryParse(data_2, out sizeHorizontal))
        {
            if (sizeHorizontal <= 0)
            {
                Console.WriteLine("\nSize cannot be <= 0");
                Console.ReadKey();
            }
        }
        else
        {
            Console.WriteLine("\nError: you can only enter numbers");
            Console.ReadKey();
        }

        int[,] workingArray = GetRandomMatrix(sizeDiagonal, sizeHorizontal);
        Console.WriteLine("Matrix - m , Snake - s");
        string choice = Console.ReadLine();

        if (choice == "m")
        {
            MatrixTrace(workingArray);
        }
        else if (choice == "s")
        {
            Console.Clear();
            Snake snake = new Snake();

            MatrixTrace(workingArray);
            snake.Snakes(workingArray);
        }
        else
        {
            Console.WriteLine("Error: Or - m, or - s");
            return;
        }


    }
    private int [,] GetRandomMatrix(int sizeDiagonal, int sizeHorizontal)
    {

        int[,] array = new int[sizeDiagonal, sizeHorizontal];

        for (int i = 0; i < sizeDiagonal; i++)
        {
            for (int j = 0; j < sizeHorizontal; j++)
            {
                array[i, j] = new Random().Next(1, 99);
            }
        }
        return array;
    }

    private int [,] MatrixTrace(int [,] array)
    {
        for (int i = 0; i < array.GetLength(0); i++)
        {
            for (int j = 0; j < array.GetLength(1); j++)
            {
                if(i == j)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.Write($"{array[i, j]}\t");
                }
                else
                {
                    Console.ResetColor();
                    Console.Write($"{array[i, j]}\t");
                }
            }
            Console.WriteLine("\n");
        }
        return array;
    }
}
